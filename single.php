<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Astrid
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();
                        echo "<div style='padding:0px 2% 0px 2%;'>";
		?>
			<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-9548287659827512" data-ad-slot="6284197976" data-ad-format="auto" data-full-width-responsive="true"></ins>
			<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
		<?php
			echo "</div>";

			get_template_part( 'template-parts/content', get_post_format() );

			the_post_navigation();
			
			echo "<div style='padding:0px 2% 0px 2%;'>";
		?>
                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-9548287659827512" data-ad-slot="1922512643" data-ad-format="auto" data-full-width-responsive="true"></ins>
                <script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
		<?php
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				echo "<div id=\"comment\"></div>";
			endif;
			echo "</div>";

		endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
if ( get_theme_mod('fullwidth_single', 0) != 1 ) :
	get_sidebar();
endif;
get_footer();
?>

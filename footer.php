<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astrid
 */

?>

		</div>
	</div><!-- #content -->

	<div class="footer-wrapper">
		<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
			<?php get_sidebar('footer'); ?>
		<?php endif; ?>
		
		<?php $toggle_contact = get_theme_mod('toggle_contact_footer', 1); ?>
		<?php if ( $toggle_contact ) : ?>
		<div class="footer-info">
			<div class="container">
				<?php astrid_footer_branding(); ?>
				<?php astrid_footer_contact(); ?>
			</div>
		</div>
		<?php endif; ?>

		<footer id="colophon" class="site-footer" role="contentinfo">	
			<div class="site-info container">
				<nav id="footernav" class="footer-navigation" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'footer', 'depth' => '1', 'menu_id' => 'footer-menu' ) ); ?>
				</nav><!-- #site-navigation -->
				<div class="site-copyright">
					<a href="https://beian.miit.gov.cn/">赣ICP备2021000211号-1</a><br/>
					<?php echo '&copy; 2017 - '.date( 'Y' ); ?> <a href="https://about.limstash.com">limstash</a> All rights reserved <br/>
					<?php do_action('astrid_footer'); ?>
				</div>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div>

</div><!-- #page -->
<script>
MathJax = {
    showProcessingMessages: false,
    messageStyle: "none",
    tex: {
        inlineMath:  [ ["$", "$"] ],
        displayMath: [ ["$$","$$"] ]
    },
    options: {
        renderActions: {
            addMenu: [0]
        }
    }
};
</script>
<script src="//cdn-static.limstash.com/mathjax/tex-mml-chtml.js"></script>
<?php wp_footer(); ?>
<script src="//cdn-static.limstash.com/disqus/iDisqus.min.js"></script>
<script>
var disq = new iDisqus('comment', {
    forum: 'limstash',
    api: 'https://apiv2.limstash.com/disqus',
    site: 'https://www.limstash.com',
    mode: 1,
    timeout: 3000,
    init: true
});
</script>
<script>
function getHitokoto() {
        jQuery.ajax({
                url: "https://hitokoto.limstash.com",
                dataType: "jsonp",
                async: true,
                jsonp: "callback",
                jsonpCallback: "hitokoto",
                success: function(result) {
                        jQuery('#hitokoto').html(result.hitokoto)
                },
                error: function() {
                        jQuery('#hitokoto').html("好像哪里有些不对劲...")
                }
        });
}
getHitokoto()
</script>
<script>
jQuery(function ($) {
	totalheight = parseFloat($(window).scrollTop());
    if(totalheight != 0) {
		var header = $('.site-header');
		header.addClass('header-hidden');
    }
});
</script>
</body>
</html>
